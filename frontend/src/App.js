import React, { Component } from "react";
import "./App.css";
import Table from 'react-bootstrap/Table'
import axios from 'axios';

const BASE_URL = 'http://127.0.0.1:8989/'
const SEARCH_EMPLOYEE_API = 'employee/'

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      seach_name: "",
      isLoading: true,
    };
  }
  load_data = () => {
    var url = BASE_URL + SEARCH_EMPLOYEE_API + (this.state.seach_name !== "" ? ("?name=" + this.state.seach_name) : "")
    axios.get(url)
      .then(res => {
        this.setState({ users: res.data, isLoading: false });
      })
  }
  search = (event) => {
    this.setState({ seach_name: event.target.value, isLoading: true }, this.load_data);
  };
  componentDidMount() { this.load_data() }
  render() {
    return (
      <div className="container ">
        <h1>Employees</h1><br/>
        <h5>Search By Name:</h5>
        <input
          type="text"
          name="seach_name"
          className="form-control form-control-lg"
          onChange={this.search}
          value={this.state.seach_name}
        /><br/>

        <Table striped bordered>
          <thead>
            <tr>
              <th>Full Name</th>
              <th>Age</th>
              <th>Gender</th>
            </tr>
          </thead>
          {this.state.isLoading ? (
            <tr><td colspan="3">Loading ...</td></tr>
          ) : (
              this.state.users.length == 0 ? (
                <tr><td colspan="3">No data found</td></tr>
              ) : (<tbody>
                {
                  this.state.users.map((user) => {
                    return (
                      <tr>
                        <td>{user.name}</td>
                        <td>{user.age}</td>
                        <td>{user.gender}</td>
                      </tr>
                    )
                  })
                }
              </tbody>)
            )}
        </Table>
      </div>
    );
  }
}
export default App;