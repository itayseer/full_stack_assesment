from django.db import models

# Create your models here.


class Employee(models.Model):

    MALE = 0
    FEMALE = 1

    GENDER_TYPES = ((MALE, "Male"), (FEMALE, "Female"))

    name = models.CharField(max_length=200)
    age = models.IntegerField()
    gender = models.IntegerField(
        choices=GENDER_TYPES, default=MALE)

    def __str__(self):
        return self.name
