from users.models import Employee
from users.serializers import EmployeeSerializer
from rest_framework import generics
# Create your views here.


class SearchEmployee(generics.ListAPIView):
    serializer_class = EmployeeSerializer

    def get_queryset(self):
        queryset = Employee.objects.filter()

        name = self.request.query_params.get('name', None)
        if name:
            queryset = queryset.filter(name__contains=name)

        return queryset
