from rest_framework import serializers
from .models import *


class EmployeeSerializer(serializers.ModelSerializer):
    gender = serializers.SerializerMethodField()

    class Meta:
        model = Employee
        fields = ('__all__')

    def get_gender(self, obj):
        return obj.get_gender_display()
